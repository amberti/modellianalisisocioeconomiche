X <- read.csv2("datiaffitti.csv")

summary(X)

names(X) <- c("ID", "Data", "UltimaPag", "Lingua", "Seme",
              "Studente", "Dividi", "DivQuante", "Affitto", 
              "Spese", "Utenze", "Contr", "Dist10",
              "Servizi", "Stabile", "Periodo", "Citta")

summary(X)

X$DivQuante <- ifelse(X$DivQuante == "", NA, X$DivQuante)
X$DivQuante <- ifelse(is.na(X$DivQuante) & X$Dividi == "NO", 1, X$DivQuante)
X$DivQuante <- ordered(X$DivQuante)
levels(X$DivQuante)
levels(X$DivQuante) <- c("1", "2", "3", "4", "5", "6+")

TmpLev <- levels(X$Spese)
X$Spese <- ordered(ifelse(X$Spese == "", NA, X$Spese))
levels(X$Spese) <- TmpLev[-1]

TmpLev <- levels(X$Utenze)
X$Utenze <- ordered(ifelse(X$Utenze == "", NA, X$Utenze))
levels(X$Utenze) <- TmpLev[-1]

levels(X$Contr)
X$Contr <- factor(ifelse(X$Contr == "", NA, X$Contr))
levels(X$Contr) <- c("Altre", "Annuale")

levels(X$Dist10) <- c("DoNotA", "NO", "SI")

TmpLev <- levels(X$Servizi)
X$Servizi <- factor(ifelse(X$Servizi == "", NA, X$Servizi))
levels(X$Servizi) <- TmpLev[-1]

TmpLev <- levels(X$Stabile)
X$Stabile <- factor(ifelse(X$Stabile == "", NA, X$Stabile))
levels(X$Stabile) <- c("Civ", "Eco", "Sig")

TmpLev <- levels(X$Periodo)
X$Periodo <- factor(ifelse(X$Periodo == "", NA, X$Periodo))
levels(X$Periodo) <- c("Ant", "Ini", "gg")

TmpLev <- levels(X$Citta)
X$Citta <- factor(ifelse(X$Citta == "", NA, X$Citta))
levels(X$Citta) <- c("Est", "Ita", "BO", "Par", "TO")

X <- X[-(1:5)]

summary(X)

X[X$Studente == "NO",]
mosaicplot(~ Studente + Affitto, X)

table(~ Studente + DivQuante + Affitto, X)
with(X, table(DivQuante, Affitto, Studente))

library(CHAID)
plot(chaid(Affitto ~ ., data = X))
plot(chaid(Affitto ~ ., data = subset(X, Citta == "TO")))
X$Pred <- predict(chaid(Affitto ~ ., data = X), newdata = X)

with(X, table(Pred, Affitto, Studente))
