Introduction to Tree-based Methods
========================================================
author: Daniele Amberti
date: September 2018
width: 1440
height: 1080

Introduction to Tree-based Methods
========================================================

Decision trees can be used for both regression and classification problems. Here we focus on classification trees. Classification trees are a hierarchical way of partitioning the space. We start with the entire space and recursively divide it into smaller regions. At the end, every region is assigned with a class label. A tree is represented with nodes:
- node, any of the nodes in the tree. Each node correspond to a region in the original space
- leaf or terminal node. Each leaf is assigned a class

See [PennState, STAT 897D, Tree-based Methods ](https://onlinecourses.science.psu.edu/stat857/node/22/)


CHI-SQUARED for classification tree (1 of 8)
========================================================

```{r}
install.packages("CHAID", repos="http://R-Forge.R-project.org")
library(CHAID); library(car)
USvote$marstat <- recode(USvote$marstat, "'married' = 'marri'; else = 'other'")
USvote$educr <- recode(USvote$educr, "c('<HS', 'HS', '>HS') = 'low'; else = 'high'")
USvote$ager <- recode(USvote$ager, "c('55-64', '65+') = 'old'; else = 'young'")
USvote$empstat <- NULL
summary(USvote)

```


CHI-SQUARED for classification tree (2 of 8)
========================================================

```{r}
tab_voteGender <- xtabs( ~ vote3 + gender, data=USvote)
tab_voteGender
(test_gender <- chisq.test(tab_voteGender))





# tab_voteMarstat <- xtabs( ~ vote3 + marstat, data=USvote)
# chisq.test(tab_voteMarstat)
# 
# 
# 
# chaidUS <- chaid(vote3 ~ marstat + educr + ager + gender, data = USvote, control = ctrl)
#   ### fit tree to subsample
#   set.seed(290875)
#   USvoteS <- USvote[sample(1:nrow(USvote), 1000),]
# 
#   ctrl <- chaid_control(minsplit = 200, minprob = 0.1)
#   chaidUS <- chaid(vote3 ~ ., data = USvoteS, control = ctrl)
# 
#   print(chaidUS)
#   plot(chaidUS)
```

CHI-SQUARED for classification tree (3 of 8)
========================================================

```{r}
(test_ager <- chisq.test(xtabs( ~ vote3 + ager, data=USvote)))
(test_educr <- chisq.test(xtabs( ~ vote3 + educr, data=USvote)))
```

CHI-SQUARED for classification tree (4 of 8)
========================================================

```{r}
(test_marstat <- chisq.test(xtabs( ~ vote3 + marstat, data=USvote)))
```

The table below summarize the results of testing indipendence of several catecorical variables against the vote and therefore we decide to make a first split over $marstat$.
```{r echo = FALSE, results = 'asis'}
library(knitr)
kable(data.frame(
  X_squared = c(test_gender$statistic, test_ager$statistic, test_educr$statistic, test_marstat$statistic), 
  p_value = c(test_gender$p.value, test_ager$p.value, test_educr$p.value, test_marstat$p.value), 
  row.names = c("gender", "ager", "educr", "marstat")), 
      caption = "X-squared and p values")
```

CHI-SQUARED for classification tree (5 of 8)
========================================================

```{r}
xtabs( ~ vote3 + marstat, data=USvote)
```

We now subset the $marstat = marri$ observations $(#6133)$ and iterate again the procedure obtaining table below. Therefore we decide to make a first split over $marstat = marri$ and over $gender$.

```{r echo = FALSE, results = 'asis'}

sub_USvote <- subset(USvote, marstat == "marri")
test_gender <- chisq.test(xtabs( ~ vote3 + gender, data=sub_USvote))
test_ager <- chisq.test(xtabs( ~ vote3 + ager, data=sub_USvote))
test_educr <- chisq.test(xtabs( ~ vote3 + educr, data=sub_USvote))
test_marstat <- chisq.test(xtabs( ~ vote3 + marstat, data=sub_USvote))

library(knitr)
kable(data.frame(
  X_squared = c(test_gender$statistic, test_ager$statistic, test_educr$statistic, test_marstat$statistic), 
  p_value = c(test_gender$p.value, test_ager$p.value, test_educr$p.value, test_marstat$p.value), 
  row.names = c("gender", "ager", "educr", "marstat")), 
      caption = "X-squared and p values")
```

CHI-SQUARED for classification tree (6 of 8)
========================================================

```{r}
xtabs( ~ vote3 + gender, data=sub_USvote)
```

We now encounter one of the several possible stop criterion: depth of the tree. Below some metrics about our leafs. 

In *leaf* $marstat = marri \cap gender = male$ we have `r round(prop.table(xtabs( ~ vote3 + gender, data=sub_USvote), margin = 2)[2,1], 2)` probability of voting Bush. In *leaf* $marstat = marri \cap gender = female$ we have `r round(prop.table(xtabs( ~ vote3 + gender, data=sub_USvote), margin = 2)[2,2], 2)` probability of voting Bush.

CHI-SQUARED for classification tree (7 of 8)
========================================================

```{r}
xtabs( ~ vote3 + marstat, data=USvote)
```

We now go back to our first split and subset the $marstat = other$ observations $(#4512)$ and iterate again the procedure obtaining table below. Therefore we decide to make a second split over $marstat = other$ and again over $gender$.

```{r echo = FALSE, results = 'asis'}

sub_USvote <- subset(USvote, marstat == "other")
test_gender <- chisq.test(xtabs( ~ vote3 + gender, data=sub_USvote))
test_ager <- chisq.test(xtabs( ~ vote3 + ager, data=sub_USvote))
test_educr <- chisq.test(xtabs( ~ vote3 + educr, data=sub_USvote))
test_marstat <- chisq.test(xtabs( ~ vote3 + marstat, data=sub_USvote))

library(knitr)
kable(data.frame(
  X_squared = c(test_gender$statistic, test_ager$statistic, test_educr$statistic, test_marstat$statistic), 
  p_value = c(test_gender$p.value, test_ager$p.value, test_educr$p.value, test_marstat$p.value), 
  row.names = c("gender", "ager", "educr", "marstat")), 
      caption = "X-squared and p values")
```

CHI-SQUARED for classification tree (8 of 8)
========================================================

```{r}
xtabs( ~ vote3 + gender, data=sub_USvote)
```

We now encounter again the stop criterion: depth of the tree. Below some metrics about our leafs. 

In *leaf* $marstat = other \cap gender = male$ we have `r round(prop.table(xtabs( ~ vote3 + gender, data=sub_USvote), margin = 2)[1,1], 2)` probability of voting Gore. In *leaf* $marstat = other \cap gender = female$ we have `r round(prop.table(xtabs( ~ vote3 + gender, data=sub_USvote), margin = 2)[1,2], 2)` probability of voting Gore.

It is worth noting that this second branch is more capable than the first branch with *Gini impurity* being respectively `r 1 - (0.62)^2` and `r 1 - (0.45)^2`.


CHAID CHi-squared Automatical Interaction Detection
========================================================

```{r}
print(tree <- chaid(vote3 ~ ., data = USvote, control = chaid_control(maxheight = 2)))
```

CHAID CHi-squared Automatical Interaction Detection
========================================================

```{r, echo=FALSE }
plot(tree)
```

***

See help file of the function *chaid* for more details about the implementation: `?chaid` in the *R* console or visit <https://r-forge.r-project.org/scm/viewvc.php/pkg/man/chaid.Rd?view=markup&root=chaid>

References
========================================================
- PennState, STAT 897D, Tree-based Methods. https://onlinecourses.science.psu.edu/stat857/node/22/
- CHi-squared Automated Interaction Detection, implementation details. https://r-forge.r-project.org/scm/viewvc.php/pkg/man/chaid.Rd?view=markup&root=chaid or `?chaid` in the *R* console